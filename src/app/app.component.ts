import { Component, AfterViewInit, OnInit } from '@angular/core';

declare var $: any;

@Component( {
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
} )

export class AppComponent implements OnInit, AfterViewInit {
  title = 'AvailabilityCalendar';

  checkSlotAvailabilityData = [
    {
      "Date": "2016-05-18",
      "HoursAvailable": [9, 10, 11, 12, 13, 14, 17]
    },
    {
      "Date": "2016-05-19",
      "HoursAvailable": [9, 10, 11, 12, 13, 14, 15, 16, 17]
    },
    {
      "Date": "2016-05-20",
      "HoursAvailable": [9, 10, 14, 15, 16, 17]
    },
    {
      "Date": "2016-05-21",
      "HoursAvailable": [9, 10, 11, 12, 13]
    },
    {
      "Date": "2016-05-23",
      "HoursAvailable": [13, 14, 15, 16]
    },
    {
      "Date": "2016-05-24",
      "HoursAvailable": [11, 12, 15, 16, 17]
    }
  ];

  date = new Date('2016-05-18T11:27:00');
  currentDay;
  month = ( this.date.getMonth() + 1 );
  currentTime = this.date.getHours();
  available = 'Available';
  unavailable = 'Unavailable';
  full = 'Full';
  slotFull = false;
  days;

  constructor () {
    if(this.month < 10){
      this.currentDay = this.date.getFullYear() + '-' + 0 + ( this.date.getMonth() + 1) + '-' + this.date.getDate();
    } else {
      this.currentDay = this.date.getFullYear() + '-' + ( this.date.getMonth() + 1) + '-' + this.date.getDate();
    }
  }

  ngOnInit (): void {
    $( document ).ready( () => {
      // slider
      $( function () {
        $( "#slider-range-max" ).slider( {
          range: "max",
          min: 1,
          max: 5,
          value: 1,
          slide: function ( event, ui ) {
            $( "#amount" ).val( ui.value );
          }
        } );
        $( "#amount" ).val( $( "#slider-range-max" ).slider( "value" ) );
      } );
    } );
  }

  ngAfterViewInit () {
    this.createTable();
  }

  getDays () {
    for ( let i = 0; i < this.checkSlotAvailabilityData.length; i++ ) {
      this.days = i;
    }
    return this.days;
  }

  createTable () {
    let days, row, column, table, tableElem, rowElem, colElem;
    days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

    row = 10;
    column = ( this.getDays() + 2 );

    table = document.querySelector( '.table' );
    tableElem = document.createElement( 'table' );

    for ( let i = 0; i < row; i++ ) {
      rowElem = document.createElement( 'tr' );

      for ( let j = 0; j < column; j++ ) {
        colElem = document.createElement( 'td' );
        rowElem.appendChild( colElem );

        if ( j > 0 && i > 0 ) {
          let currentSlot: string;
          switch ( i ) {
            case 1: currentSlot = this.checkSlotAvailability( 9, 0, this.checkSlotAvailabilityData[( j - 1 )]["Date"], this.checkSlotAvailabilityData[( j - 1 )]["HoursAvailable"] );
              colElem.innerHTML = currentSlot;
              this.setStyling( currentSlot, colElem, i, j );
              break;
            case 2: currentSlot = this.checkSlotAvailability( 10, 0, this.checkSlotAvailabilityData[( j - 1 )]["Date"], this.checkSlotAvailabilityData[( j - 1 )]["HoursAvailable"] );
              colElem.innerHTML = currentSlot;
              this.setStyling( currentSlot, colElem, i, j );
              break;
            case 3: currentSlot = this.checkSlotAvailability( 11, 0, this.checkSlotAvailabilityData[( j - 1 )]["Date"], this.checkSlotAvailabilityData[( j - 1 )]["HoursAvailable"] );
              colElem.innerHTML = currentSlot;
              this.setStyling( currentSlot, colElem, i, j );
              break;
            case 4: currentSlot = this.checkSlotAvailability( 12, 0, this.checkSlotAvailabilityData[( j - 1 )]["Date"], this.checkSlotAvailabilityData[( j - 1 )]["HoursAvailable"] );
              colElem.innerHTML = currentSlot;
              this.setStyling( currentSlot, colElem, i, j );
              break;
            case 5: currentSlot = this.checkSlotAvailability( 13, 0, this.checkSlotAvailabilityData[( j - 1 )]["Date"], this.checkSlotAvailabilityData[( j - 1 )]["HoursAvailable"] );
              colElem.innerHTML = currentSlot;
              this.setStyling( currentSlot, colElem, i, j );
              break;
            case 6: currentSlot = this.checkSlotAvailability( 14, 0, this.checkSlotAvailabilityData[( j - 1 )]["Date"], this.checkSlotAvailabilityData[( j - 1 )]["HoursAvailable"] );
              colElem.innerHTML = currentSlot;
              this.setStyling( currentSlot, colElem, i, j );
              break;
            case 7: currentSlot = this.checkSlotAvailability( 15, 0, this.checkSlotAvailabilityData[( j - 1 )]["Date"], this.checkSlotAvailabilityData[( j - 1 )]["HoursAvailable"] );
              colElem.innerHTML = currentSlot;
              this.setStyling( currentSlot, colElem, i, j );
              break;
            case 8: currentSlot = this.checkSlotAvailability( 16, 0, this.checkSlotAvailabilityData[( j - 1 )]["Date"], this.checkSlotAvailabilityData[( j - 1 )]["HoursAvailable"] );
              colElem.innerHTML = currentSlot;
              this.setStyling( currentSlot, colElem, i, j );
              break;
            case 9: currentSlot = this.checkSlotAvailability( 17, 0, this.checkSlotAvailabilityData[( j - 1 )]["Date"], this.checkSlotAvailabilityData[( j - 1 )]["HoursAvailable"] );
              colElem.innerHTML = currentSlot;
              this.setStyling( currentSlot, colElem, i, j );
              break;
          }
        }

        if ( i === 0 && j > 0 ) { // insert day name and date
          let d = new Date( this.checkSlotAvailabilityData[( j - 1 )]["Date"] );
          let dayName = days[d.getDay()];
          let day = d.getDate();
          colElem.innerHTML = dayName + " " + day;
        }

        if ( j === 0 ) { // insert the hours
          switch ( i ) {
            case 1: colElem.innerHTML = '09:00 - 10:00';
              break;
            case 2: colElem.innerHTML = '10:00 - 11:00';
              break;
            case 3: colElem.innerHTML = '11:00 - 12:00';
              break;
            case 4: colElem.innerHTML = '12:00 - 13:00';
              break;
            case 5: colElem.innerHTML = '13:00 - 14:00';
              break;
            case 6: colElem.innerHTML = '14:00 - 15:00';
              break;
            case 7: colElem.innerHTML = '15:00 - 16:00';
              break;
            case 8: colElem.innerHTML = '16:00 - 17:00';
              break;
            case 9: colElem.innerHTML = '17:00 - 18:00';
              break;
          }
        }

        // column styling
        colElem.style.padding = '10px';
        colElem.style.border = '1px solid lightgrey';

        if ( i === 0 ) {
          colElem.style.background = '#444444';
          colElem.style.color = 'white';
          colElem.style.borderColor = '#444444';
        }

        if ( colElem.classList.contains( 'full' ) ) {
          colElem.style.background = 'red';
          colElem.style.color = 'white';
        }

        if ( colElem.classList.contains( 'unavailable' ) ) {
          colElem.style.background = 'lightgray';
          colElem.style.color = 'darkgray';
          colElem.style.fontStyle = 'italic';
        }

        if ( colElem.classList.contains( 'available' ) ) {
          colElem.style.cursor = 'pointer';
          const tempData = this.checkSlotAvailabilityData;

          colElem.addEventListener( "click", function ( event ) {
            let component = new AppComponent();
            let sliderValue = document.getElementById( 'amount' )['value'];
            let row = event.target.getAttribute( 'row' );
            let column = event.target.getAttribute( 'column' );
            let currentData = tempData[( column - 1 )];

            const selected = document.querySelectorAll( '.selected' );
            for ( let o = 0; o < selected.length; o++ ) { //remove selected when clicked again
              selected[o].classList.remove( 'selected' );
              selected[o]['style'].background = 'unset';
              selected[o]['style'].color = 'black';
              selected[o].innerHTML = 'Available';
            }

            if ( component.checkSlotAvailability( parseInt( row ) + 8, sliderValue, currentData['Date'], currentData['HoursAvailable'] ) === 'Available' ) {
              for ( let x = 0; x < sliderValue; x++ ) { // add selected class to all required slots
                let currentSlot = document.querySelector( `.row-${ parseInt( row ) + x }-column-${ parseInt( column ) }` );
                currentSlot.classList.add('selected' );
                currentSlot['style'].background = 'green';
                currentSlot['style'].color = 'white';
                currentSlot.innerHTML = 'Selected';
              }
            }
          } );
        }

        // table styling
        tableElem.setAttribute( 'style', `
            border-spacing: 0;
        `);
      }

      tableElem.appendChild( rowElem );
      table.appendChild( tableElem );
    }
  }

  setStyling ( availabilty: string, element, row, column ) {
    element.setAttribute( 'row', row );
    element.setAttribute( 'column', column );
    element.classList.add( `row-${ row }-column-${ column }` );

    switch ( availabilty ) {
      case "Full": element.classList.add( 'full' );
        break;
      case "Unavailable": element.classList.add( 'unavailable' );
        break;
      case "Available": element.classList.add( 'available' );
        break;
    }
  }

  checkSlotAvailability ( time, jobLength, date, availabilty ) {
    this.slotFull = false;

    for ( let i = 0; i < this.checkSlotAvailabilityData.length; i++ ) {
      if ( this.checkSlotAvailabilityData[i]["Date"] === date && this.checkSlotAvailabilityData[i]["Date"] === this.currentDay ) { // check if date equals current date
        if ( ( time - 2 ) <= this.currentTime ) { // check if time is in the past of the current day (before buffer included)
          return this.full;
        }
        else {
          if ( !availabilty.includes( ( time ) ) ) { // check if time provided is not in the data
            return this.full;
          }
          else if ( !availabilty.includes( ( time - 1 ) ) ) { // check if the slot itself appears in the data but there is not enough buffer before it
            if ( time === 9 ) { // 9 do not require buffer before
              if ( jobLength > 0 ) {
                for ( let p = 1; p <= jobLength; p++ ) { // check if the job length will run over to a slot that is full or past 17
                  if ( !availabilty.includes( ( time + p ) ) ) {
                    this.slotFull = true;
                  }
                }
                if ( this.slotFull ) {
                  return this.unavailable;
                } else {
                  return this.available;
                }
              } else {
                return this.available;
              }
            } else {
              return this.unavailable;
            }
          }
          else if ( !availabilty.includes( ( time + 1 ) ) ) { // check if the slot itself appears in the data but there is not enough buffer after it
            return this.checkBufferAfter( time, jobLength );
          }

          this.checkSlotFull( jobLength, availabilty, time );
        }
      }
      else if ( this.checkSlotAvailabilityData[i]["Date"] === date ) { // not current date, only 1 hour buffer before and after
        if ( !availabilty.includes( ( time ) ) ) { // check if time provided is not in the data
          return this.full;
        }
        else if ( !availabilty.includes( ( time - 1 ) ) ) { // check if the slot itself appears in the data but there is not enough buffer before it
          if ( time === 9 ) { // 9 do not require buffer before
            if ( jobLength > 0 ) {
              for ( let p = 1; p <= jobLength; p++ ) { // check if the job length will run over to a slot that is full or past 17
                if ( !availabilty.includes( ( time + p ) ) ) {
                  this.slotFull = true;
                }
              }
              if ( this.slotFull ) {
                return this.unavailable;
              } else {
                return this.available;
              }
            } else {
              return this.available;
            }
          } else {
            return this.unavailable;
          }
        }
        else if ( !availabilty.includes( ( time + 1 ) ) ) { // check if the slot itself appears in the data but there is not enough buffer after it
          return this.checkBufferAfter( time, jobLength );
        }

        for ( let p = 1; p <= jobLength; p++ ) { // check if the job length will run over to a slot that is full or past 17
          if ( !availabilty.includes( ( time + p ) ) ) {
            if ( ( time + p ) === 18 ) { // 17 do not require buffer after but can only accept job length of one
              this.slotFull = false;
              continue;
            }
            this.slotFull = true;
          }
        }

        if ( this.slotFull ) {
          return this.unavailable;
        } else {
          return this.available;
        }
      }
    }
  }

  checkBufferAfter ( time, jobLength ) {
    if ( time === 17 && jobLength <= 1 ) { // 17 do not require buffer after
      return this.available;
    } else if ( time === 17 && jobLength > 1 ) {
      return this.unavailable;
    }
    return this.unavailable;
  }

  checkSlotFull ( jobLength, availabilty, time ) {
    for ( let p = 1; p <= jobLength; p++ ) { // check if the job length will run over to a slot that is full or past 17
      if ( !availabilty.includes( ( time + p ) ) ) {
        if ( ( time + p ) === 18 ) { // 17 do not require buffer after but can only accept job length of one
          this.slotFull = false;
          continue;
        }
        this.slotFull = true;
      }
    }

    if ( this.slotFull ) {
      return this.unavailable;
    } else {
      return this.available;
    }
  }
}
